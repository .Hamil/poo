package Encapsulamiento;

public abstract class Personaje {

    //aqui ponemos nuestros atributos en private para que solo se puedan modificar desde su clase, normalmente esto se hace para evitar errores
    // Encapsulamiento es el proceso de ocultar los detalles de implementacion de un objeto (atributos privados), exponiendo solo sus interfaces
    // y solo se puede acceder o modificar atraves de getter and setter
    private String nombre;
    private int edad;
    private String textura;
    private String poder;

    public Personaje(){
        setPoder("Ninguno");
    }

    public String getPoder() {
        return poder;
    }

    protected void setPoder(String poder){ //lo ponemos protected para poder acceder a el solo desde sus hijos y los archivos del mismo paquete
        this.poder = poder;
    }

    public int getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTextura() {
        return textura;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTextura(String textura) {
        this.textura = textura;
    }

    public abstract void saludar();
}
