package Abstraccion;

public class AnimalHerbivoro extends Animal {

    @Override
    public void alimentarse() {
        System.out.println("El Abstracion.Animal Herviboro se alimenta de hierba");
    }
}
