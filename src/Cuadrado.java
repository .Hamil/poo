public class Cuadrado implements FiguraGeometrica{
    private float lado;

    public Cuadrado(float lado) {
        this.lado = lado;
    }

    public float getLado() {
        return lado;
    }

    public void setLado(float lado) {
        this.lado = lado;
    }

    @Override
    public float calcularArea() {
        return lado * lado;
    }

    public void imprimirDimensiones(){
        System.out.println("Los lados tienen rectangulos: "+ lado);
    }
}
