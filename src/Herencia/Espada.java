package Herencia;

// La Herencia en programacion es la capacidad de crear clases que hereden de otras clases, esto nos ayuda a reutilizar codigo y a no tener que crear metodos que no se van a utilizar
public class Espada {
    private String material;
    private String Forma;
    private String Color;

    public Espada(String material, String Forma, String Color){
        this.material = material;
        this.Forma = Forma;
        this.Color = Color;
    }

    public String getColor() {
        return Color;
    }

    public String getForma() {
        return Forma;
    }

    public String getMaterial() {
        return material;
    }

    public void mostrarDatos(){
        System.out.println("Espada: "
                + "\nForma: " +  this.getForma()
                + "\nColor: " + this.getColor()
                + "\nMaterial: " + this.getMaterial()
        );
    }
}

