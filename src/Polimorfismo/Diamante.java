package Polimorfismo;

public class Diamante extends Cubo {
    protected String premio;

    public Diamante(int dimension, String name, String textura){
        super(dimension, name, textura);
        this.premio = "Diamante";
    }

    @Override
    public void destruir() {
        System.out.println("Me acabo de romper, ten un "+this.premio);
    }
}
