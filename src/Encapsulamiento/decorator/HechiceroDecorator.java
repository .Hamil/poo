package Encapsulamiento.decorator;

import Encapsulamiento.Personaje;

public class HechiceroDecorator extends PersonajeDecorator {

    public HechiceroDecorator(IPersonaje persona) {
        super(persona);
        this.setPoder("Hechicero");
    }

    public void saludar() {

        System.out.println("Soy un Hechicero, mis datos son: " +
                "\nNombre: " + this.persona.getNombre() +
                "\nEdad: " + this.persona.getEdad() +
                "\nTextura: " + this.persona.getTextura() +
                "\nPoder: Hechicero");
    }
}
