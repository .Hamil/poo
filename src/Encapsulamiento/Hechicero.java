package Encapsulamiento;

public class Hechicero extends Personaje{
    private String poder;

    public String getPoder() {
        return poder;
    }

    public void setPoder(String color) {
        this.poder = poder;
    }

    @Override
    public void saludar() {

        System.out.println("Soy un Hechicero, mis datos son: " +
                "\nNombre: " + this.getNombre() +
                "\nEdad: " + this.getEdad() +
                "\nTextura: " + this.getTextura() +
                "\nPoder: " + getPoder());
    }
}
