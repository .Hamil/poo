public class Rectangulo extends Cuadrado{
    private float alto;

    public Rectangulo(float lado, float alto) {
        super(lado);
        this.alto = alto;
    }

    public float getAlto() {
        return alto;
    }

    public void setAlto(float alto) {
        this.alto = alto;
    }

    @Override
    public float calcularArea() {
        return getLado() * alto;
    }
}
