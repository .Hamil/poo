package Encapsulamiento.decorator;

public class basePersonaje implements IPersonaje {

    protected String nombre;
    protected int edad;
    protected String textura;
    protected String poder;

    @Override
    public void setPoder(String poder) {
        this.poder = poder;
    }

    @Override
    public void setTextura(String textura) {
        this.textura = textura;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String getPoder() {
        return this.poder;
    }

    @Override
    public String getTextura() {
        return this.textura;
    }

    @Override
    public String getNombre() {
        return this.nombre;
    }

    @Override
    public int getEdad() {
        return this.edad;
    }

    @Override
    public void saludar() {
        System.out.println("Soy un personaje basico sin super poderes :(");
    }
}
