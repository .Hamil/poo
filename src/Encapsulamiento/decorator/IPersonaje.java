package Encapsulamiento.decorator;

import Encapsulamiento.Personaje;

public interface IPersonaje {
    void setPoder(String poder);
    void setTextura(String textura);
    void setNombre(String nombre);
    void setEdad(int edad);
    String getPoder();
    String getTextura();
    String getNombre();
    int getEdad();
    void saludar();
}
