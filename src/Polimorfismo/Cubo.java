package Polimorfismo;

// Polimorfismo en programacion es cuando sobreescribimos un metodo de una clase padre en una clase hija
public class Cubo {
    protected int dimension;
    protected String name;
    protected String textura;

    public Cubo(int dimension, String name, String textura){
        this.dimension = dimension;
        this.name = name;
        this.textura = textura;
    }

    public void destruir(){
        System.out.println("Me acabo de destruir, pero no se que hacer porque soy el padre de las clases que si tienen identidad");
    }

    public void mostrarInformacion(){
        System.out.println("Dimension: " + this.dimension +
                            "\nTextura: " + this.textura +
                            "\nName: " + this.name);
    }
}
