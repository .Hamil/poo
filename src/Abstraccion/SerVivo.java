package Abstraccion;

//La abstraccion en programacion es la capacidad de crear clases y metodos abstractos para que solo se puedan utilizar en sus hijos
// Por que usar la abstraccion? Porque en Java no se puede heredar de mas de una clase, pero si se puede heredar de una clase abstracta y de una clase normal
// Diferencia entre una clase abstracta y una clase normal? Una clase abstracta no se puede instanciar, pero si se puede heredar de ella
// Diferencia entre un metodo abstracto y un metodo normal? Un metodo abstracto no tiene cuerpo, pero si se puede sobreescribir en las clases hijas
// Diferencia entre una interfaz y una clase abstracta? Una interfaz no tiene atributos, pero si metodos abstractos, una clase abstracta si tiene atributos y metodos abstractos
// Al Java ser un lenguaje que no permite la herencia multiple, las interfaces son una buena opcion para poder heredar de mas de una clase
// Si quieres heredar de mas de 2 clases, puedes hacerlo con una clase abstracta y varias interfaces o solo con interfaces (esta es la ventaja principal de la interfaces que puedes heredar de mas de una clase)
public abstract class SerVivo {
    public abstract void alimentarse(); // como este metodo es abstracto no podemos definirlo (no podemos escribir dentro de el)
}
