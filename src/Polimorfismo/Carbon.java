package Polimorfismo;

public class Carbon extends Cubo{
    protected String premio;

    public Carbon(int dimension, String name, String textura){
        super(dimension, name, textura);
        this.premio = "Carbon";
    }

    @Override
    public void destruir() { //como estamos utilizando polimofirsmo, cada metodo de estos se llamara igual en cada una de las clases, pero haran cosas distintas
        System.out.println("Me acabo de romper, ten un "+this.premio);
    }

}
