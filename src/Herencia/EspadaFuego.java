package Herencia;

public class EspadaFuego extends Espada {
    private String Efecto;

    public EspadaFuego(String material, String Forma, String Color, String Efecto) {
        super(material, Forma, Color);
        this.Efecto = Efecto;
    }

    public String getEfecto() {
        return Efecto;
    }

    public void mostrarDatos(){
        System.out.println("Espada de fuego: "
                + "\nForma: " +  this.getForma()
                + "\nColor: " + this.getColor()
                + "\nMaterial: " + this.getMaterial()
                + "\nEfecto: " + this.getEfecto()
        );
    }
}
