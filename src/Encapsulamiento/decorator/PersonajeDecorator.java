package Encapsulamiento.decorator;
import  Encapsulamiento.Personaje;

public abstract class PersonajeDecorator implements IPersonaje{
    protected IPersonaje persona; //lo ponemos protected para tener acceso desde sus hijos y desde el mismo paquete

    protected String nombre;
    protected int edad;
    protected String textura;
    protected String poder;

    public PersonajeDecorator(IPersonaje persona){
        this.persona = persona;
    }

    public void setPoder(String poder) {
        this.poder = poder;
    }

    public void setTextura(String textura) {
        this.textura = textura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getPoder() {
        return poder;
    }

    public String getTextura() {
        return textura;
    }

    public String getNombre() {
        return nombre;
    }

    public int getEdad() {
        return edad;
    }

    public abstract void saludar();


}
