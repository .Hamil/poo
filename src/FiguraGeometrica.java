public interface FiguraGeometrica {
    float calcularArea();
}
