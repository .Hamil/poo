import Abstraccion.AnimalCarnivoro;
import Abstraccion.Planta;
import Encapsulamiento.decorator.PersonajeDecorator;
import Herencia.Espada;
import Herencia.EspadaFuego;
import Polimorfismo.Cubo;
import Polimorfismo.Carbon;
import Polimorfismo.Diamante;
import Encapsulamiento.*;
import Encapsulamiento.decorator.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args){
        List <String> objeto = new ArrayList<>();
        objeto.add("Hola");
        objeto.add("Hola2");
        objeto.add("Hola3");

        List <String> variable = objeto.stream().map((value) -> "tutorail " + value).collect(Collectors.toList());
//        basePersonaje personaje = new basePersonaje();
//        personaje.setEdad(10);
//        personaje.setNombre("Julian");
//        personaje.setTextura("Suave");
//        HechiceroDecorator hechicero = new HechiceroDecorator(personaje);
//        hechicero.saludar();
    }
}
